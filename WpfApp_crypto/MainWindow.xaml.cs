﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace WpfApp_crypto
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        private MainRequest _main = new MainRequest();
        private C_Crypto _crypto = new C_Crypto();
        private OpenFileDialog _openFileDialog = new OpenFileDialog();

        public MainWindow()
        {
            InitializeComponent();

            _main.CertInput(ListBoxCert);
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            _openFileDialog = _main.SelectAndOpenFile(TextBoxOut);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            _main.SelectAndSaveFile(TextBoxOut);
        }

        private void BtnCrypt_Click(object sender, RoutedEventArgs e)
        {
            _main.EncryptFile(ListBoxCert, ListBoxCrypt, TextBoxOut, TextBoxKey, _openFileDialog, ComboBoxKeySize, ComboBoxDinam,
                _crypto);
        }

        private void BtnDecrypt_Click(object sender, RoutedEventArgs e)
        {
            _main.DecryptFile(ListBoxCert, ListBoxCrypt, ComboBoxDinam, TextBoxOut, _crypto);
        }

        private void BtnKey_Click(object sender, RoutedEventArgs e)
        {
            _crypto.CreatePrivateKey(TextBoxKey, ComboBoxKeySize);
        }

        private void ListBoxItem_Selected_des(object sender, RoutedEventArgs e)
        {
            _main.DesKeyChange(ComboBoxKeySize, ComboBoxDinam);
        }

        private void ListBoxItem_Selected_aes(object sender, RoutedEventArgs e)
        {
            _main.AesKeyChange(ComboBoxKeySize, ComboBoxDinam);
        }

        private void ListBoxItem_Selected_two(object sender, RoutedEventArgs e)
        {
            _main.TwoFishKeyChange(ComboBoxKeySize, ComboBoxDinam);
        }
    }
}
