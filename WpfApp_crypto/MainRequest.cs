﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using ClrPlus.Crypto;
using Microsoft.Win32;

namespace WpfApp_crypto
{
    public class MainRequest
    {
        public OpenFileDialog SelectAndOpenFile(TextBox textBox)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Выберите текстовый файл";
            if (openFileDialog.ShowDialog() == true)
                textBox.Text = File.ReadAllText(openFileDialog.FileName);
            //MessageBox.Show(Convert.ToString(openFileDialog.FileName));
            return openFileDialog;
        }

        public void SelectAndSaveFile(TextBox textBox)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Выберите путь сохранения";
            if (saveFileDialog.ShowDialog() == true)
                File.WriteAllText(saveFileDialog.FileName + ".txt", textBox.Text);
        }

        public void DesKeyChange(ComboBox cb, ComboBox comboBoxDinam)
        {
            comboBoxDinam.Items.Clear();
            comboBoxDinam.Items.Add("CBC");
            comboBoxDinam.Items.Add("ECB");
            cb.Items.Clear();
            cb.Items.Add("8");
        }

        public void AesKeyChange(ComboBox cb, ComboBox comboBoxDinam)
        {
            comboBoxDinam.Items.Clear();
            cb.Items.Clear();
            cb.Items.Add("128");
            cb.Items.Add("192");
            cb.Items.Add("256");
        }

        public void TwoFishKeyChange(ComboBox cb, ComboBox comboBoxDinam)
        {
            comboBoxDinam.Items.Clear();
            cb.Items.Clear();
            cb.Items.Add("128");
            cb.Items.Add("256");
        }

        public void CertInput(ListBox listBox)
        {
            try
            {
                X509Store store = new X509Store("My");
                store.Open(OpenFlags.ReadOnly);
                foreach (X509Certificate2 mCert in store.Certificates)
                {
                    //listBox.Items.Add(mCert.GetNameInfo(X509NameType.SimpleName, true));
                    //listBox.Items.Add(mCert.GetNameInfo(X509NameType.SimpleName, false));
                    listBox.Items.Add(mCert.GetName());
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public void EncryptFile(ListBox listBoxCert, ListBox listBoxCrypt, TextBox textBoxOut, TextBox textBoxKey,
            OpenFileDialog openFileDialog, ComboBox comboBoxKeySize, ComboBox comboBoxDinam, C_Crypto _crypto)
        {
            // на всякий случай сохраняем текстовый файл
            //File.WriteAllText(openFileDialog.FileName, textBoxOut.Text);

            // генерируемным сессионным ключём шифруем файл открытый в редактор
            // des, aes, twofish
            if (listBoxCrypt.SelectedItem != null && listBoxCert.SelectedItem != null)
            {
                //var certKey;
                X509Store store = new X509Store("My");
                store.Open(OpenFlags.ReadOnly);
                foreach (X509Certificate2 mCert in store.Certificates)
                {
                    if (mCert.GetName() == listBoxCert.SelectedItem.ToString())
                    {
                        // *DES
                        if (listBoxCrypt.SelectedIndex == 0)
                        {
                            SaveFileDialog saveFileDialog = new SaveFileDialog();
                            saveFileDialog.Title = "Выберите путь сохранения зашифрованного текста";
                            if (saveFileDialog.ShowDialog() == true)
                                File.WriteAllText(saveFileDialog.FileName + ".txt",
                                    _crypto.DES_Encrypt(textBoxKey.Text, comboBoxDinam.SelectedIndex, openFileDialog));
                            //_crypto.DES_Encrypt(mCert.GetPublicKeyString(), textBoxKey.Text, openFileDialog, int.Parse(comboBoxKeySize.Text));
                        }
                        // *AES
                        if (listBoxCrypt.SelectedIndex == 1)
                        {
                            SaveFileDialog saveFileDialog = new SaveFileDialog();
                            saveFileDialog.Title = "Выберите путь сохранения зашифрованного текста";
                            if (saveFileDialog.ShowDialog() == true)
                                File.WriteAllText(saveFileDialog.FileName + ".txt",
                                    _crypto.AES_Encrypt(textBoxKey.Text, openFileDialog));
                        }
                        // *Two
                        if (listBoxCrypt.SelectedIndex == 2)
                        {

                        }

                        // *
                        SaveFileDialog saveFileDialogKey = new SaveFileDialog();
                        if (saveFileDialogKey.ShowDialog() == true)
                            File.WriteAllText(saveFileDialogKey.FileName + ".txt", 
                            _crypto.EncryptPrivateKey(mCert, textBoxKey.Text));
                    }
                }
            }
        }

        public void DecryptFile(ListBox listBoxCert, ListBox listBoxCrypt, ComboBox comboBoxDinam, TextBox textBoxOut, C_Crypto _crypto)
        {
            // генерируемным сессионным ключём шифруем файл открытый в редактор
            // des, aes, twofish
            if (listBoxCrypt.SelectedItem != null && listBoxCert.SelectedItem != null)
            {
                //var certKey;
                X509Store store = new X509Store("My");
                store.Open(OpenFlags.ReadOnly);
                foreach (X509Certificate2 mCert in store.Certificates)
                {
                    if (mCert.GetName() == listBoxCert.SelectedItem.ToString())
                    {
                        // выбираем файл с закрытым ключём
                        string privatKeyE = "";
                        string privatKey = "";
                        string  privatText = "";
                        //OpenFileDialog openFileDialog = new OpenFileDialog { Title = "Выберите приватный ключ" };
                        OpenFileDialog openFileDialog = new OpenFileDialog();
                        openFileDialog.Title = "Выберите приватный ключ";

                        if (openFileDialog.ShowDialog() == true)
                            privatKeyE = File.ReadAllText(openFileDialog.FileName);
                        privatKey = _crypto.DecryptPrivateKey(mCert, privatKeyE);

                        openFileDialog.Title = "Выберите зашифрованный текст";
                        if (openFileDialog.ShowDialog() == true)
                            privatText = File.ReadAllText(openFileDialog.FileName);

                        // *DES
                        if (listBoxCrypt.SelectedIndex == 0)
                        {
                            SaveFileDialog saveFileDialog = new SaveFileDialog();
                            if (saveFileDialog.ShowDialog() == true)
                                File.WriteAllText(saveFileDialog.FileName + ".txt",
                                    _crypto.DES_Decrypt(privatText, comboBoxDinam.SelectedIndex, privatKey));
                            //_crypto.DES_Encrypt(mCert.GetPublicKeyString(), textBoxKey.Text, openFileDialog, int.Parse(comboBoxKeySize.Text));
                        }
                        // *AES
                        if (listBoxCrypt.SelectedIndex == 1)
                        {
                            SaveFileDialog saveFileDialog = new SaveFileDialog();
                            if (saveFileDialog.ShowDialog() == true)
                                File.WriteAllText(saveFileDialog.FileName + ".txt",
                                    _crypto.AES_Decrypt(privatText, privatKey));
                        }
                        // *Two
                        if (listBoxCrypt.SelectedIndex == 2)
                        {

                        }
                    }
                }
            }
        }
    }
}