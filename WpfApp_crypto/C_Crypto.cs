﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;

namespace WpfApp_crypto
{
    public class C_Crypto
    {
        #region CryptoMethods

        #region DES

        public string DES_Encrypt(string privateKey, int modeValue, OpenFileDialog openFile)
        {
            byte[] b_key = Encoding.ASCII.GetBytes(privateKey);
            byte[] b_iv = Encoding.ASCII.GetBytes(privateKey);

            string originalString = File.ReadAllText(openFile.FileName);

            if (String.IsNullOrEmpty(originalString))
            {
                throw new ArgumentNullException
                    ("The string which needs to be encrypted can not be null.");
            }

            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();

            if (modeValue == 0)
                cryptoProvider.Mode = CipherMode.CBC;
            if (modeValue == 1)
                cryptoProvider.Mode = CipherMode.ECB;

            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(b_key, b_iv),
                CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cryptoStream);

            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();

            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int) memoryStream.Length);
        }

        public string DES_Decrypt(string cryptedString, int modeValue, string privateKey)
        {
            byte[] b_key = Encoding.UTF8.GetBytes(privateKey);
            byte[] b_iv = Encoding.UTF8.GetBytes(privateKey);

            if (String.IsNullOrEmpty(cryptedString))
            {
                throw new ArgumentNullException
                    ("The string which needs to be decrypted can not be null.");
            }
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();

            if (modeValue == 0)
                cryptoProvider.Mode = CipherMode.CBC;
            if (modeValue == 1)
                cryptoProvider.Mode = CipherMode.ECB;

            MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
            CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(b_key, b_iv),
                CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);

            return reader.ReadToEnd();
        }

        #endregion

        #region AES

        public string AES_Encrypt(string privateKey, OpenFileDialog openFile)
        {
            string originalString = File.ReadAllText(openFile.FileName);

            // Создаем объект класса AES
            // с определенным ключом and iv.
            Aes aesAlg = Aes.Create();

            aesAlg.Key = Encoding.ASCII.GetBytes(privateKey);
            aesAlg.IV = Encoding.ASCII.GetBytes(privateKey);

            // Создаем поток для шифрования.
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV),
                CryptoStreamMode.Write);
            var writer = new StreamWriter(cryptoStream);

            //Записываем в поток все данные.
            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();

            //Возвращаем зашифрованные байты из потока памяти.
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int) memoryStream.Length);
        }

        public string AES_Decrypt(string cryptedString, string privateKey)
        {
            // Создаем объект класса AES,
            // Ключ и iv
            Aes aesAlg = Aes.Create();

            aesAlg.Key = Encoding.UTF8.GetBytes(privateKey);
            aesAlg.IV = Encoding.UTF8.GetBytes(privateKey);

            // Создаем поток для расшифрования.
            var memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
            var cryptoStream = new CryptoStream(memoryStream, aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV),
                CryptoStreamMode.Read);
            var reader = new StreamReader(cryptoStream);

            return reader.ReadToEnd();
        }

        #endregion

        #endregion

        #region Other

        public void CreatePrivateKey(TextBox textBoxKey, ComboBox comboBoxKeySize)
        {
            try
            {
                var stringBuilder = new StringBuilder();
                for (var i = 0; i < int.Parse(comboBoxKeySize.Text); i++)
                {
                    stringBuilder.Append(Convert.ToString(Srand(0, 9)));
                }
                textBoxKey.Text = stringBuilder.ToString();
            }
            catch
            {
                MessageBox.Show("Choose key size value first");
            }
        }

        public string EncryptPrivateKey(X509Certificate2 x509, string stringToEncrypt)
        {
            if (x509 == null || string.IsNullOrEmpty(stringToEncrypt))
                throw new Exception("A x509 certificate and string for encryption must be provided");

            RSACryptoServiceProvider rsa = (RSACryptoServiceProvider) x509.PublicKey.Key;
            byte[] bytestoEncrypt = Encoding.ASCII.GetBytes(stringToEncrypt);
            byte[] encryptedBytes = rsa.Encrypt(bytestoEncrypt, false);
            return Convert.ToBase64String(encryptedBytes);
        }

        // retrieves the maximum number of characters that can be decrypted at once
        private int GetMaxBlockSize(int keySize)
        {
            int max = ((int) (keySize/8/3))*4;
            if (keySize/8%3 != 0)
            {
                max += 4;
            }
            return max;
        }

        public string DecryptPrivateKey(X509Certificate2 x509, string stringTodecrypt)
        {
            if (x509 == null || string.IsNullOrEmpty(stringTodecrypt))
                throw new Exception("A x509 certificate and string for decryption must be provided");

            if (!x509.HasPrivateKey)
                throw new Exception("x509 certicate does not contain a private key for decryption");

            RSACryptoServiceProvider rsa = (RSACryptoServiceProvider) x509.PrivateKey;
            //StringBuilder decryptBuilder = new StringBuilder();

            //int maxDecryptSize = GetMaxBlockSize(rsa.KeySize);
            //int iterationCount = (int) Math.Floor((double) (stringTodecrypt.Length/maxDecryptSize));
            //for (int i = 0; i < iterationCount; i++)
            //{
            //    int start = i * maxDecryptSize;
            //    int blkSize = Math.Min(start + maxDecryptSize, stringTodecrypt.Length);
            //    byte[] msgBytes = Convert.FromBase64String(stringTodecrypt.Substring(start, blkSize));

            //    decryptBuilder.Append(Encoding.Unicode.GetString(rsa.Decrypt(msgBytes, false)));
            //}
            //return decryptBuilder.ToString();

            byte[] bytestodecrypt = Convert.FromBase64String(stringTodecrypt);
            byte[] plainbytes = rsa.Decrypt(bytestodecrypt, false);

            ASCIIEncoding enc = new ASCIIEncoding();
            return enc.GetString(plainbytes);
        }

        #endregion

        #region RealRand

        private byte Srand(int firstValue, int secondValue)
        {
            var rand = new RNGCryptoServiceProvider();
            byte[] data = new byte[1];
            do
            {
                rand.GetBytes(data);
            } while ((data[0] < firstValue) || (data[0] > secondValue));
            return (data[0]);
        }

        #endregion

        #region Хеширование MD5 

        private string EncryptMd5(string plainText)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(plainText));
            return (BitConverter.ToString(checkSum1).Replace("-", string.Empty));
        }

        #endregion
    }
}
